import 'package:exercise/ViewModelProvider/ViewModelProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'viewmodel/session_viewmodel.dart';
import 'widgets/session_text_field.dart';

class SessionScreen extends StatefulWidget {
  @override
  _SessionScreenState createState() => _SessionScreenState();
}

class _SessionScreenState extends State<SessionScreen> {
  SessionViewModel viewModel;

  @override
  void initState() {
    super.initState();
    viewModel = ViewModelProvider.of<SessionViewModel>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery
              .of(context)
              .size
              .height,
          width: MediaQuery
              .of(context)
              .size
              .width,
          decoration: BoxDecoration(color: Colors.white),
          child: Form(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 25,),
                  Padding(
                    padding: const EdgeInsets.all(18),
                    child: SessionTextFormField(
                      "Total Training Intervals",
                      textInputType: TextInputType.emailAddress,
                      focusNode: viewModel.totalIntervalFocus,
                      controller: viewModel.totalIntervalController,
                      errorText: "invalid number",
                      textCapitalization: TextCapitalization.none,
                      keyboardType: TextInputType.number,
                      onFieldSubmitted: (_) {
                        viewModel.totalIntervalFocus.unfocus();
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext builder) {
                              return Container(
                                  height: MediaQuery.of(context).copyWith().size.height / 3,
                                  child: viewModel.setTInterval());
                            });
                      },
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(1),
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                      icon: Icon(Icons.add),
                    ),
                  ),
                  GestureDetector(
                        onTap:() {
                      showModalBottomSheet(
                          context: context,
                          builder: (BuildContext builder) {
                            return Container(
                                height: MediaQuery.of(context).copyWith().size.height / 3,
                                child: viewModel.setTInterval());
                          });
                    },
                    child: Container(
                      padding: const EdgeInsets.all(18),
                      child: SessionTextFormField(
                        "Training Interval Time",
                        textInputType: TextInputType.emailAddress,enable: false,
                        focusNode: viewModel.trainingIntervalFocus,
                        controller: viewModel.trainingIntervalController,
//                      errorText: "invalid interval",
                        textCapitalization: TextCapitalization.none,
                        onFieldSubmitted: (_) {
                          nextFocus(viewModel.trainingIntervalFocus,
                              viewModel.breakIntervalFocus, context);
                        },
                        icon: Icon(Icons.stop),
                      ),
                    ),
                  ),
                  GestureDetector(
                        onTap:() {
                      showModalBottomSheet(
                          context: context,
                          builder: (BuildContext builder) {
                            return Container(
                                height:
                                MediaQuery.of(context).copyWith().size.height /
                                    3,
                                child: viewModel.setBInterval());
                          });
                    },
                    child: Container(
                      padding: const EdgeInsets.all(18),
                      child: SessionTextFormField(
                        "Break Interval Time",
                        textInputType: TextInputType.emailAddress,
                        focusNode: viewModel.breakIntervalFocus,
                        controller: viewModel.breakIntervalController,enable: false,
//                      errorText: "invalid interval",
                        textCapitalization: TextCapitalization.none,
                        onFieldSubmitted: (_) {
                          nextFocus(viewModel.trainingIntervalFocus,
                              viewModel.breakIntervalFocus, context);
                        },
                        icon: Icon(Icons.stop),
                      ),
                    ),
                  ),
                  RaisedButton(
                    onPressed: (){
                      viewModel.buttonFunction(context);
                      },
                    padding: const EdgeInsets.all(18.0),
                    child: Text("Submit",style: TextStyle(color: Colors.black,fontSize: 18),
                    ),
                  )
                ],
              )
          ),
        ),
      ),
    );
  }

  Widget time(TextEditingController controller, Duration duration) {
    return CupertinoTimerPicker(
      mode: CupertinoTimerPickerMode.hms,
      minuteInterval: 1,
      secondInterval: 1,
      onTimerDurationChanged: (Duration changedTimer) {
        controller.text = changedTimer.toString();
        controller.notifyListeners();
        duration = changedTimer;
      },
    );
  }

  void nextFocus(FocusNode from, FocusNode to, context) {
    from.unfocus();
    FocusScope.of(context).requestFocus(to);

    @override
    void dispose() {
      viewModel.dispose();
      super.dispose();
    }
  }
}