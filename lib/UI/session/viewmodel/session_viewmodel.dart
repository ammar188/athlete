

import 'package:exercise/Models/user.dart';
import 'package:exercise/UI/util/duration_format_mask.dart';
import 'package:exercise/ViewModelProvider/ViewModelProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../routes.dart';

class SessionViewModel extends ViewModelBase {


  // Durations
  Duration trainingDuration = Duration();
  Duration breakDuration = Duration();
  //Controllers
  final totalIntervalController = TextEditingController();
  final trainingIntervalController = TextEditingController();
  final breakIntervalController = TextEditingController();

  //Focus Node
  final totalIntervalFocus = FocusNode();
  final trainingIntervalFocus = FocusNode();
  final breakIntervalFocus = FocusNode();

  SessionViewModel();

  Widget setTInterval() {
    return CupertinoTimerPicker(
      mode: CupertinoTimerPickerMode.ms,
      minuteInterval: 1,
      secondInterval: 1,
      onTimerDurationChanged: (Duration changedTimer) {
        trainingIntervalController.text = changedTimer.toMinutesSeconds();
        trainingIntervalController.notifyListeners();
        trainingDuration = changedTimer;
      },
    );
  }
  Widget setBInterval() {
    return CupertinoTimerPicker(
      mode: CupertinoTimerPickerMode.ms,
      minuteInterval: 1,
      secondInterval: 1,
      onTimerDurationChanged: (Duration changedTimer) {
        breakIntervalController.text = changedTimer.toMinutesSeconds();
        breakIntervalController.notifyListeners();
        breakDuration = changedTimer;
      },
    );
  }
  buttonFunction(context){
    print(trainingIntervalController.text);
    if(totalIntervalController.text== "")
      showMessage(context,"Please enter #No of intervals.");
    else if(trainingIntervalController.text == "" || trainingIntervalController.text == "00:00")
      showMessage(context,"Please enter Training duration.");
    else if(breakIntervalController.text == "" || trainingIntervalController.text == "00:00")
      showMessage(context,"Please enter Break duration.");
    else
    Navigator.of(context).pushNamed(AppRoutes.ROOT, arguments: {
      "session": Session(totalIntervals: int.parse(totalIntervalController.text),bInterval: breakDuration,tInterval: trainingDuration),
    });
  }
  @override
  void dispose(){
    print("SessionViewModel disposing");
  }
  showMessage(context,message){
    showDialog(
        context: context,
        builder: (context) => new CupertinoAlertDialog(
          title:Column(children: <Widget>[
            Text("Alert",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 25),maxLines: 2,textAlign: TextAlign.center,),
            SizedBox(height: 30,),
          ],),
          content: new Text(message,style: TextStyle(color: Colors.grey,fontSize: 15)),
          actions: <Widget>[
            Center(
              child: new FlatButton(
                child: new Text("OK",style: TextStyle(fontSize: 20),),
                textColor: Colors.black,
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ),

          ],
        )

    );
  }
}
