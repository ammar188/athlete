import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SessionTextFormField extends StatefulWidget {
  final String textLabel;
  final Widget icon;
  final String errorText;
  final TextEditingController controller;
  final FocusNode focusNode;
  final Function onFieldSubmitted;
  final TextInputAction textInputAction;
  final TextInputType textInputType;
  final TextCapitalization textCapitalization;
  final bool obscureText;
  final bool isRed;
  final String Function(String password) validator;
  final int maxLines;
  final List<TextInputFormatter> inputFormatters;
  final String hintText;
  final double fontSize;
  final Color textColor;
  final Color backgroundColor;
  final Color shadowColor;
  final FocusNode nextFocusNode;
  final FormFieldValidator<String> onChangeFocusValidate;
  final TextInputType keyboardType;
  final String errorMsg;
  final RegExp regexValidator;
  final String validatorMsgError;
  final Duration validateAfter;
  final bool enable;
  final int maxLength;
  final Widget labelWidget;

  @override
  SessionTextFormFieldState createState() {
    return new SessionTextFormFieldState();
  }

  SessionTextFormField(this.textLabel,
      {this.icon,
        this.errorText,
        this.controller,
        FocusNode focusNode,
        this.onFieldSubmitted,
        this.textInputAction,
        this.textInputType,
        this.textCapitalization,
        this.obscureText,
        this.isRed = false,
        this.validator,
        this.maxLines,
        this.inputFormatters,
        this.hintText,
        this.fontSize,
        this.textColor,
        this.backgroundColor,
        this.shadowColor,
        this.nextFocusNode,
        this.onChangeFocusValidate,
        this.keyboardType,
        this.errorMsg,
        this.regexValidator,
        this.validatorMsgError,
        this.validateAfter,
        this.enable = true,
        this.maxLength,
        this.labelWidget})
      : this.focusNode = focusNode ?? FocusNode();
}

class SessionTextFormFieldState extends State<SessionTextFormField> {
  String _errorMsg;

  @override
  void initState() {
    super.initState();
    widget.focusNode.addListener(_changeFocus);
    if (widget.regexValidator != null)
      widget.controller.addListener(_validateRegExp);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(border: Border.all(color:Colors.grey ,width: 0.5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            widget.labelWidget == null ?
            Text(
              widget.textLabel,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 14,color: Color(0xff9e9e9e)),
            ):widget.labelWidget,
            TextFormField(
              style: TextStyle(
                  fontSize: 18),
              focusNode: widget.focusNode,
              textCapitalization:
              widget.textCapitalization ?? TextCapitalization.none,
              controller: widget.controller,
              textInputAction: widget.textInputAction ?? TextInputAction.next,
              onFieldSubmitted: widget.onFieldSubmitted ?? _nextField,
              inputFormatters: widget.inputFormatters,
              obscureText: widget.obscureText ?? false,
              keyboardType: widget.textInputType,
              validator: widget.validator,
              decoration: InputDecoration.collapsed(
                  hintText: widget.hintText,
                  hintStyle: TextStyle(fontSize: 20, color: Color(0xff9e9e9e))),
              maxLines: widget.maxLines ?? 1,
              enabled: widget.enable ?? true,
              maxLength: widget.maxLength,
            )
          ],
        ));
  }

  void _validateRegExp() {
    Future.delayed(widget.validateAfter, () {
      if (mounted)
        setState(() {
          if (!widget.regexValidator.hasMatch(widget.controller.text)) {
            _errorMsg = widget.validatorMsgError;
          } else {
            _errorMsg = null;
          }
        });
    });
  }

  void _nextField(String s) {
    if (widget.nextFocusNode != null) {
      widget.focusNode.unfocus();
      FocusScope.of(context).requestFocus(widget.nextFocusNode);
    }
  }

  void _changeFocus() {
    if (mounted)
      setState(() {
        if (!widget.focusNode.hasFocus &&
            widget.onChangeFocusValidate != null) {
          _errorMsg =
              widget.onChangeFocusValidate(widget.controller.text) ?? null;
        }
      });
  }

}
