import 'package:dioc/dioc.dart' as dioc;
import 'package:exercise/ViewModelProvider/ViewModelProvider.dart';
import 'package:flutter/material.dart';
import '../main.dart';
import 'main/main_screen.dart';
import 'main/viewmodel/main_viewmodel.dart';
import 'session/sessionPage.dart';
import 'session/viewmodel/session_viewmodel.dart';

class AppRoutes {

  static const String ROOT = "/root";
  static const String SESSION = "/session";

  static get routes => {
        AppRoutes.ROOT: (context) {
          var viewModel = c.get<MainViewModel>(
              creator: "MainViewModel", mode: dioc.InjectMode.create);
          if(ModalRoute.of(context).isCurrent) {
            Map args = ModalRoute.of(context).settings.arguments;
            viewModel..addSession(args["session"]);
          }
          return ViewModelProvider<MainViewModel>(
            child: MainScreen(),
            viewmodel: viewModel,
          );
        },
        AppRoutes.SESSION: (context) {
          var viewModel = c.get<SessionViewModel>(
              creator: "SessionViewModel", mode: dioc.InjectMode.create);
          return ViewModelProvider<SessionViewModel>(
            child: SessionScreen(),
            viewmodel: viewModel,
          );
        },
      };
}
