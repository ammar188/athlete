import 'dart:async';

import 'package:exercise/Data/comment/comment_data_source.dart';
import 'package:exercise/Models/user.dart';
import 'package:exercise/ViewModelProvider/ViewModelProvider.dart';
import 'package:rxdart/rxdart.dart';

class MainViewModel extends ViewModelBase {

  final CommentsDataSource _commentsDataSource;

  final BehaviorSubject<String> _commentSubject = BehaviorSubject();
  Stream<String> get outComment => _commentSubject.stream;

  final BehaviorSubject<bool> _isPlaying = BehaviorSubject();
  Stream<bool> get outIsPlaying => _isPlaying.stream;

  final BehaviorSubject<bool> _isFinished = BehaviorSubject();
  Stream<bool> get outIsFinished => _isFinished.stream;

  final BehaviorSubject<bool> _isTraininig = BehaviorSubject();
  Stream<bool> get outIsTraining => _isTraininig.stream;

  final BehaviorSubject<Duration> _timeLeft = BehaviorSubject();
  Stream<Duration> get outTimeString => _timeLeft.stream;
  final BehaviorSubject<int> _currentRound = BehaviorSubject();
  Stream<int> get outCurrentRound => _currentRound.stream;

  Session session;
  
  addSession(Session session){
    print("sesseion is $session");
    this.session = session;
    _currentRound.sink.add(1);
    _timeLeft.sink.add(this.session.tInterval);
    _isTraininig.sink.add(true);
  }

  Timer timer;
  play(){
    timer = Timer.periodic(
      Duration(seconds: 1),
          (Timer timer) => _timeLeft.sink.add(Duration(seconds:_timeLeft.value.inSeconds-1))
    );
    _isPlaying.sink.add(true);
  }
  pause(){
    timer.cancel();
    _isPlaying.sink.add(false);
  }
  stop(){
    _timeLeft.sink.add(Duration(seconds: 0));
  }
  MainViewModel(this._commentsDataSource){
    _commentsDataSource.todaysComment().then((str){
      _commentSubject.sink.add(str);
    });
    _isFinished.sink.add(false);
    _timeLeft.listen((duration){
      if(duration.inSeconds<=0){
        if(_isTraininig.value==true){
          _timeLeft.sink.add(session.bInterval);
          _isTraininig.sink.add(false);
          if(_currentRound.value==this.session.totalIntervals){
            timer.cancel();
            _isFinished.sink.add(true);
            print("finished");
          }
        }
        else {
          _timeLeft.sink.add(session.tInterval);
          _isTraininig.sink.add(true);
          _currentRound.sink.add(_currentRound.value+1);
        }
      }
    });
    _isPlaying.sink.add(false);
  }

  @override
  void dispose(){
    print("mainViewModel disposing");
  }

}
