import 'package:exercise/ViewModelProvider/ViewModelProvider.dart';
import 'package:flutter/material.dart';
import 'viewmodel/main_viewmodel.dart';
import 'widgets/counter.dart';
import 'widgets/playButton.dart';
import 'package:exercise/UI/util/duration_format_mask.dart';

import 'widgets/roundNo.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>{
  MainViewModel viewModel;

  @override
  void initState() {
    super.initState();
    viewModel = ViewModelProvider.of<MainViewModel>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: StreamBuilder<bool>(
          stream: viewModel.outIsFinished,
          builder: (context, snapshotF) {
            if(snapshotF.hasData && snapshotF.data != true)
            return StreamBuilder<bool>(
                stream: viewModel.outIsTraining,
                builder: (context, snapshotT) {
                  if(snapshotT.hasData)
                    return Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      decoration: (snapshotT.data)?BoxDecoration(color: Colors.white):BoxDecoration(color: Colors.red),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          StreamBuilder<int>(
                              stream: viewModel.outCurrentRound,
                              builder: (context, snapshot) {
                                return Container(
                                  width: 150,
                                  child: RoundNumberWidget(
                                    displayStr: snapshot?.data?.toString()??" ",
                                  ),
                                );
                              }
                          ),
                          Padding(padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.1),),
                          StreamBuilder<Duration>(
                              stream: viewModel.outTimeString,
                              builder: (context, snapshot) {
                                if(snapshot.hasData)
                                  return Container(
                                      width: MediaQuery.of(context).size.width*0.8,
                                      height: 150,
                                      padding: EdgeInsets.only(top: 3.0, right: 4.0),
                                      child:CountDownWidget(displayStr: snapshot.data.toMinutesSeconds())
                                  );
                                else
                                  return Container(width: 60.0, height: 60,);
                              }
                          ),
                          Padding(padding: EdgeInsets.all(MediaQuery.of(context).size.height*0.1),),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[

                              StreamBuilder<bool>(
                                  stream: viewModel.outIsPlaying,
                                  builder: (context, snapshot) {
                                    return PlayPauseButton(
                                      icon: (snapshot.hasData && snapshot.data) ?
                                      Icon(Icons.pause,size: 40,):
                                      Icon(Icons.play_arrow,size: 40,),
                                      enabled: snapshot.hasData && snapshot.data?true:true,
                                      fun: (){
                                        if(snapshot.hasData && !snapshot.data)
                                          viewModel.play();
                                        else
                                          viewModel.pause();
                                      },
                                    );
                                  }
                              ),
                              PlayPauseButton(
                                icon: Icon(Icons.stop,size: 40,),
                                enabled: snapshotT.data??false,
                                fun: (){
                                  viewModel.stop();
                                },
                              ),
                            ],
                          ),

                          StreamBuilder(
                            stream: viewModel.outComment,
                            builder: (bContext, snap) {
                              return
                                Container(
                                    padding: EdgeInsets.all(2),
                                    height: MediaQuery.of(context).size.height * 0.04,
                                    child: Text(snap?.data ?? "Comment, coming up",style: TextStyle(color: Colors.black),)
                                );
                            },
                          ),
                        ],
                      ),
                    );
                  else
                    return Container();
                }
            );
            else
              return Container(child: Center(child: Text("Some finished widget")),);
          }
      ),
    );
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

}
