
import 'package:flutter/material.dart';

class PlayPauseButton extends StatefulWidget {
  final Widget icon;
  final bool enabled;
  final fun;
  @override
  PlayPauseButtonState createState() {
    return new PlayPauseButtonState();
  }

  PlayPauseButton({@required this.icon, @required this.enabled, @required this.fun});
}

class PlayPauseButtonState extends State<PlayPauseButton> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: widget.enabled?widget.fun:null,
//      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
//    ,
      padding: const EdgeInsets.all(0.0),
      child: Ink(
          width: MediaQuery.of(context).size.width*0.4,
          height: MediaQuery.of(context).size.width*0.2,
          decoration: BoxDecoration(
            gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[widget.enabled?Colors.grey[100]:Colors.grey[900], widget.enabled?Colors.grey[600]:Colors.grey[400]],
          ),
          ),
          child: widget.icon
      ),
    );
  }

}
