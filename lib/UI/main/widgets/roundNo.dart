
import 'package:flutter/material.dart';

class RoundNumberWidget extends StatefulWidget {
  String displayStr;

  @override
  RoundNumberWidgetState createState() {
    return RoundNumberWidgetState();
  }

  RoundNumberWidget({@required this.displayStr});
}

class RoundNumberWidgetState extends State<RoundNumberWidget>{


  @override
  void initState() {
    super.initState();
    widget.displayStr = widget?.displayStr+"Round";
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return new Container(
//      height: 70,

      color: Colors.yellow,
      child: Center(child: Text(widget.displayStr + "Round",style: TextStyle(color: Colors.white,fontSize: 30),)),
    );
  }

}
