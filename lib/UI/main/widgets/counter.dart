
import 'package:flutter/material.dart';

class CountDownWidget extends StatefulWidget {
  String displayStr;

  @override
  CountDownWidgetState createState() {
    return CountDownWidgetState();
  }

  CountDownWidget({@required this.displayStr});
}

class CountDownWidgetState extends State<CountDownWidget>{


  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Text(widget?.displayStr??"",style: TextStyle(color: Colors.black,fontSize: 100,fontFamily: "CenturyGothic",),),
    );
  }

}
