import 'package:flutter/material.dart';

class AppColors {
  static const Color black = Colors.black;

  static const MaterialColor backGround = const MaterialColor(0xff2ebeb, const {
    100: const Color(0xffffffff),
    300: const Color(0xffffffff),
    500: const Color(0xffffffff),
    700: const Color(0xffffffff),
    900: const Color(0xffffffff)
  });
  static const MaterialColor grey90 = const MaterialColor(0xff909090, const {
    100: const Color(0xffdcdcdc),
    300: const Color(0xff9d9d9d),
    500: const Color(0xff909090),
    700: const Color(0xff767676),
    900: const Color(0xff505050)
  });
  static const MaterialColor grey57 = const MaterialColor(0xff575654, const {
    100: const Color(0xffE3E1DB),
    200: const Color(0xffA3A29E),
    700: const Color(0xff575654),
    500: const Color(0xff646360),
    900: const Color(0xff3D3D3B),
  });
  static const MaterialColor greyD6 = const MaterialColor(0xffd6d7d9, const {
    100: const Color(0xffBDBEBF),
    200: const Color(0xffB1B1B3),
    500: const Color(0xffd6d7d9),
    700: const Color(0xff979899),
    900: const Color(0xff585959),
  });

  static const MaterialColor darkBlue = const MaterialColor(0xff323743, const {
    100: const Color(0xff9BAACF),
    200: const Color(0xff6B768F),
    700: const Color(0xff323743),
    500: const Color(0xff3C4150),
    900: const Color(0xff262A33),
  });

  static const MaterialColor orange = const MaterialColor(0xffff4c34, const {
    100: const Color(0xffBF3927),
    200: const Color(0xffE5442F),
    500: const Color(0xffff4c34),
    700: const Color(0xff7F261A),
    900: const Color(0xff40130D),
  });

  static const MaterialColor red = const MaterialColor(0xffff0000, const {
    100: const Color(0xffE50000),
    200: const Color(0xffBF0000),
    500: const Color(0xffff0000),
    700: const Color(0xff7F0000),
    900: const Color(0xff400000),
  });
}