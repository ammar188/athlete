import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as Http;
import 'package:meta/meta.dart';

class AppHttp {
  final String baseUrl;
  static final defaultHeader = <String, String>{};
  bool showLog;

  AppHttp({@required this.baseUrl, this.showLog = false});

  Future<Http.Response> get(String path,
      {Map<String, String> headers = const {},
      Map<String, String> query = const {}}) async {
    return _loadResponse(await Http.get(Uri.parse(_buildQuery(path, query)),
        headers: _mergeHeader(headers)));
  }

  Map<String, String> _mergeHeader(Map<String, String> headers) {
    var header = <String, String>{};
    return header..addAll(headers)..addAll(defaultHeader);
  }

  String _buildQuery(String path, Map<String, String> query) {
    if (query.isEmpty) return "$baseUrl$path";
    var result = "$baseUrl$path?";
    query.forEach((key, value) {
      result += "$key=$value&";
    });
    return Uri.encodeFull(result.substring(0, result.length - 1));
  }

  Future<Http.Response> _loadResponse(Http.Response response,
      [bool showLog]) async {
    if (showLog ?? this.showLog) {
      print("Request Headers: ${response.request.headers}");
      print("Status Code:${response.statusCode}"
          "\nMethod: ${response.request.method}"
          " | url: ${response.request.url}");
      print("Headers: ${response.headers}");
      print("Body:${response.body}");
    }
    return response;
  }
}
