import 'package:http/http.dart' as http;
import '../comment_data_source.dart';


class ApiCommentDataSource extends CommentsDataSource {
  final url = Uri.parse("https://pastebin.com/raw/jmhKjPLD");
  String comment = "";

  @override
  Future<String> todaysComment() async {
    var response = await http.get(url);
      if (response.statusCode != 200) return "";
      return response.body;
  }
}
