import 'dart:convert';

import 'package:exercise/Models/request_http_response.dart';
import 'package:http/http.dart';

RequestResponse error<T>(Response response) {
  if (response.statusCode >= 500)
    return RequestResponse<T>.fail(
        code: response.statusCode, errors: ['Server error']);
  final json = jsonDecode(response.body);
  return RequestResponse<T>.fail(
      code: response.statusCode,
      errors: json['errors'] is String
          ? [json['errors']]
          : json['errors'].cast<String>());
}

