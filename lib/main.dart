import 'package:flutter/material.dart';
import 'package:dioc/dioc.dart' as dioc;

import 'DI/injects.dart';
import 'UI/app_colors.dart';
import 'UI/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}
final dioc.Container c = InitDependencies().container;


class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Exercise',
        initialRoute: AppRoutes.SESSION,
        routes: AppRoutes.routes,
      theme: ThemeData(
          primarySwatch: Colors.grey,
          primaryColor: AppColors.darkBlue,
          primaryColorDark: Colors.black,
          accentColor: AppColors.orange,
          fontFamily: 'Helvetica',
          primaryTextTheme:
          TextTheme(body1: TextStyle(color: AppColors.grey90))),
    );
  }
}
