import 'package:dioc/dioc.dart';
import 'package:exercise/Data/app_http.dart';
import 'package:exercise/Data/comment/comment_data_source.dart';
import 'package:exercise/Data/comment/remote/api_comment_data_source.dart';
import 'package:exercise/UI/main/viewmodel/main_viewmodel.dart';
import 'package:exercise/UI/session/viewmodel/session_viewmodel.dart';


class InitDependencies {
  final container = Container();

  InitDependencies() {

    container.register<CommentsDataSource>(
            (c) => ApiCommentDataSource(),
        name: "CommentsDataSource");

    //ViewModels
    container.register<MainViewModel>((c) {
      print("MainViewModel registered");
      return MainViewModel(
        c.get<CommentsDataSource>(creator: "CommentsDataSource"),
      );
    }, name: "MainViewModel", defaultMode: InjectMode.create);
    container.register<SessionViewModel>((c) {
      print("SessionViewModel registered");
      return SessionViewModel();
    }, name: "SessionViewModel", defaultMode: InjectMode.create);

  }
}
