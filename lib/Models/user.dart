class Session {
  int totalIntervals;
  Duration tInterval;
  Duration bInterval;

  Session({
    this.totalIntervals,
    this.bInterval,
    this.tInterval
  });

  @override
  String toString() {
    return 'Session{totalIntervals ${totalIntervals}, tInterval ${tInterval.toString()}, bInterval ${bInterval.toString()}';
  }

//  factory Session.fromJson(Map<String, dynamic> json) {
//    return Session(
//        totalIntervals: json['total_intervals'],
//        tInterval: json["training_interval_seconds"],
//        bInterval: json["break_interval_seconds"],
//        );
//  }
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['total_intervals'] = this.totalIntervals;
//    data['training_interval_seconds'] = this.tInterval;
//    data['break_interval_seconds'] = this.bInterval;
//    return data..removeWhere((_, value) => value == null);
//  }
}