import 'package:meta/meta.dart';

class RequestResponse<T> {
  int code;
  T data;
  List<String> errors;

  bool get isSuccess => code >= 200 && code < 300;

  RequestResponse({@required this.code, this.data});

  RequestResponse.success(this.data) : code = 200;

  RequestResponse.fail({@required this.code, @required this.errors});
}
